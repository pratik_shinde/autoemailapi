//in this version, I can send image attachment with the mail successfully

package com.main.tries;

import java.io.IOException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import com.sun.mail.smtp.SMTPMessage;

public class sendEmail {
	final String senderUsername = "test.genesisportal@gmail.com";
    final String senderPassword = "Cdktest1234";

    protected Session createSession(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(senderUsername, senderPassword);
                    }
                });

        return session;
    }

    public int composeMessage(String sendTo, String nameOfTheCandidate,String testUrl,String username, String password) throws IOException {
        try {
//            MimeMessage messages = new MimeMessage(createSession());
//            messages.setFrom(new InternetAddress(username));
//            
//            messages.setSubject("CDK Global online coding test");
//            messages.setText("Hey "+nameOfTheCandidate+",Please click on the link below to take a test now..\n"+testUrl+"\nUsername : "+username+"\npassword : "+password+"\n\nAll the best.. See you soon on board..!");

            SMTPMessage message = new SMTPMessage(createSession());
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
            MimeMultipart content = new MimeMultipart();
            MimeBodyPart mainPart = new MimeBodyPart();

            mainPart.setText("Hey "+nameOfTheCandidate+",Please click on the link below to take a test now..\n"+testUrl+"\nUsername : "+username+"\npassword : "+password+"\n\nAll the best.. See you soon on board..!");

            content.addBodyPart(mainPart);

            message.setContent(content);

            message.setSubject("CDK Global online coding test");
            MimeBodyPart imagePart = new MimeBodyPart();

            imagePart.attachFile("/Users/shindepr/Desktop/logo.svg");

            content.addBodyPart(imagePart);
            //send the message
            Transport.send(message);

            System.out.println("message sent successfully...");

            return 1;

        } catch (MessagingException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
