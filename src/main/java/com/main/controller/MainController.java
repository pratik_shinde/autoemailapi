package com.main.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.main.service.*;

@RestController
public class MainController {
	
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public int sendEmail(@RequestParam String sendTo, @RequestParam String nameOfTheCandidate, @RequestParam String testUrl, @RequestParam String username, @RequestParam String password) throws IOException
	{
		sendEmail send = new sendEmail();
		return send.composeMessage(sendTo, nameOfTheCandidate, testUrl, username, password);
	}
}
