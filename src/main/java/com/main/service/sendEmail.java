package com.main.service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class sendEmail {
	final String senderUsername = "test.genesisportal@gmail.com";
    final String senderPassword = "Cdktest1234";

    protected Session createSession(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(senderUsername, senderPassword);
                    }
                });

        return session;
    }

    public int composeMessage(String sendTo, String nameOfTheCandidate,String testUrl,String username, String password) {
        try {
            MimeMessage message = new MimeMessage(createSession());
            message.setFrom(new InternetAddress(username));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
            message.setSubject("CDK Global online coding test");
            message.setText("Hey "+nameOfTheCandidate+",Please click on the link below to take a test now..\n"+testUrl+"\nUsername : "+username+"\npassword : "+password+"\nAll the best.. See you soon on board..!");

            
            //send the message
            Transport.send(message);

            System.out.println("message sent successfully...");

            return 1;

        } catch (MessagingException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
